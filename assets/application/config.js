import {port} from '@/utils';

export default {
    wsApiEndpoint: `ws://${window.location.hostname}:${port}/_debugger/ws/api`
}
